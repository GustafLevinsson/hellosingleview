//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Gustaf on 2018-10-24.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func ButtonClicked(_ sender: UIButton) {
        print("Hello there button")
    }
    
}

